// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EntryLevelTestGameMode.generated.h"

class AELTSpawner;

UCLASS(minimalapi)
class AEntryLevelTestGameMode : public AGameModeBase
{
	GENERATED_BODY()\
private:

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "GameMode", meta = (AllowPrivateAccess = "true"))
	AELTSpawner* Spawner;

public:
	AEntryLevelTestGameMode();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool AutoSpawnSpawner{true};

	

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintPure)
	AELTSpawner* GetSpawner();
		
};
// Copyright Epic Games, Inc. All Rights Reserved.

#include "EntryLevelTestGameMode.h"
#include "EntryLevelTestCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Gameplay/ELTSpawner.h"

AEntryLevelTestGameMode::AEntryLevelTestGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void AEntryLevelTestGameMode::BeginPlay() {
	Super::BeginPlay();

	if (AutoSpawnSpawner) {
		FActorSpawnParameters SpawnInfo;
		Spawner = GetWorld()->SpawnActor<AELTSpawner>(AELTSpawner::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator, SpawnInfo);
	}
}

AELTSpawner* AEntryLevelTestGameMode::GetSpawner()
{
	return Spawner;
}
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ELTPickUp.generated.h"

class USphereComponent;
class UStaticMeshComponent;
class ACharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPickUpSignature, AELTPickUp*, PickUp, ACharacter*, Character);

UCLASS()
class ENTRYLEVELTEST_API AELTPickUp : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AELTPickUp();

	/* Sound playing when the pick up is picked up */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "PickUp")
	USoundBase* PickUpSound;

	/* Delegate called when a character overlaps with the collision sphere of the pick up */
	UPROPERTY(BlueprintAssignable, Category = "PickUp")
	FPickUpSignature OnPickUp;

	/* Sphere collision responsible for overlap check of being picked up by a character */
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "PickUp", meta = (AllowPrivateAccess = "true"))
	USphereComponent* CollisionSphere;

	/* Mesh that can be used to display in the pick up */
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "PickUp", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* Mesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

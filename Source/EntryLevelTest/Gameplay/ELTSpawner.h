// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/CoreUObject/Public/UObject/SoftObjectPtr.h"
#include "ELTSpawner.generated.h"

class AELTPickUp;
class ACharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPickUpCollectedSignature, AELTPickUp*, PickUp, ACharacter*, Character);

UCLASS()
class ENTRYLEVELTEST_API AELTSpawner : public AActor
{
	GENERATED_BODY()

private:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, replicated, Category = "Spawner", meta = (AllowPrivateAccess = "true"))
	TArray<AELTPickUp*> PickUps;
	
public:	
	// Sets default values for this actor's properties
	AELTSpawner();

	UPROPERTY(BlueprintAssignable, Category = "Spawner")
	FPickUpCollectedSignature OnPickUpCollected;

	/* Default number of items to spawn*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Spawner")
	int32 NumPickUpsToSpawn{5};

	/* Delegate called when one of the pick ups this spawner spawned has been collected */
	UFUNCTION(NetMulticast, Reliable, Category = "Spawner")
	void PickUpCollected(AELTPickUp* PickUp, ACharacter* Character);

	/*
	 * Get all pick ups this spawner spawned
	 */
	UFUNCTION(BlueprintPure, Category = "Spawner")
	TArray<AELTPickUp*> GetPickUps();

	/*
	 * Get total number of pick ups spawned by this spawner
	 */
	UFUNCTION(BlueprintPure, Category = "Spawner")
	int32 GetPickUpsNum();

	/*
	 * Get pick ups closest to local player
	 */
	UFUNCTION(BlueprintPure, Category = "Spawner")
	AELTPickUp* GetClosestPickUp();

	/*
	 * Get pick ups in view for local player
	 */
	UFUNCTION(BlueprintPure, Category = "Spawner")
	TArray<AELTPickUp*> GetPickUpsInView();

	/*
	 * Get pick ups out of view for local player
	 */
	UFUNCTION(BlueprintPure, Category = "Spawner")
	TArray<AELTPickUp*> GetPickUpsOutOfView();

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "ELTPickUp.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AELTPickUp::AELTPickUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	bReplicates = true;
	bAlwaysRelevant = true;

	CollisionSphere = CreateOptionalDefaultSubobject<USphereComponent>(TEXT("CollisionSphere0"));
	if (CollisionSphere)
	{
		CollisionSphere->SetCollisionProfileName(TEXT("Actor"));
		CollisionSphere->SetSphereRadius(32.0f);
		CollisionSphere->SetGenerateOverlapEvents(true);
		CollisionSphere->SetCanEverAffectNavigation(false);
		CollisionSphere->SetHiddenInGame(false);
		RootComponent = Mesh;
		CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AELTPickUp::CollisionSphereBeginOverlap);
	}

	Mesh = CreateOptionalDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh0"));
	if (Mesh)
	{
		Mesh->SetCollisionProfileName(TEXT("Actor"));
		Mesh->SetGenerateOverlapEvents(true);
		Mesh->SetCanEverAffectNavigation(false);
		Mesh->SetupAttachment(CollisionSphere);
	}

	if (PickUpSound == nullptr)
	{
		static ConstructorHelpers::FObjectFinder<USoundBase> PickUpSoundObj(TEXT("'/Engine/VREditor/Sounds/UI/Dockable_Window_Close.Dockable_Window_Close'"));
		PickUpSound = PickUpSoundObj.Object;
	}

}

// Called when the game starts or when spawned
void AELTPickUp::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AELTPickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AELTPickUp::CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ACharacter* Character = Cast<ACharacter>(OtherActor);
	if (Character) {
		UGameplayStatics::PlaySound2D(GetWorld(), PickUpSound);
		OnPickUp.Broadcast(this, Character);
		this->Destroy();
	}
}

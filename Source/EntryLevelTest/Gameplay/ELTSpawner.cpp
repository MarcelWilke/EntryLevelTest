// Fill out your copyright notice in the Description page of Project Settings.


#include "ELTSpawner.h"
#include "NavigationSystem.h"
#include "ELTPickUp.h"
#include "GameFramework/Character.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/GameplayStatics.h"
#include "Math/Vector.h" 

AELTSpawner::AELTSpawner()
{
	PrimaryActorTick.bCanEverTick = false;

	bReplicates = true;
	bAlwaysRelevant = true;
}

void AELTSpawner::BeginPlay()
{
	Super::BeginPlay();

	UNavigationSystemV1* NavSystem = UNavigationSystemV1::GetCurrent(GetWorld());
	if (!NavSystem)
		return;

	int32 ItemsToSpawn{ NumPickUpsToSpawn };
	int32 MaxCount{ 100 };
	while (ItemsToSpawn > 0 && MaxCount > 0)
	{
		FNavLocation SpawnPoint;
		bool bSuccess = NavSystem->GetRandomPoint(SpawnPoint);
		if (bSuccess) {
			FActorSpawnParameters SpawnInfo;
			AELTPickUp* PickUp = GetWorld()->SpawnActor<AELTPickUp>(AELTPickUp::StaticClass(), SpawnPoint.Location, FRotator::ZeroRotator, SpawnInfo);
			PickUp->OnPickUp.AddDynamic(this, &AELTSpawner::PickUpCollected);
			PickUps.Add(PickUp);
			--ItemsToSpawn;
		}
		--MaxCount;
	}
}

void AELTSpawner::PickUpCollected_Implementation(AELTPickUp* PickUp, ACharacter* Character)
{
	PickUps.Remove(PickUp);
	OnPickUpCollected.Broadcast(PickUp, Character);
}

TArray<AELTPickUp*> AELTSpawner::GetPickUps()
{
	return PickUps;
}


int32 AELTSpawner::GetPickUpsNum()
{
	return PickUps.Num();
}

AELTPickUp* AELTSpawner::GetClosestPickUp()
{
	AELTPickUp* ClosestPickUp = nullptr;

	if (PickUps.Num() > 0)
	{
		ACharacter* Player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
		if (Player == nullptr) {
			UE_LOG(LogTemp, Error, TEXT("No player found"));
			return ClosestPickUp;
		}

		ClosestPickUp = PickUps[0];
		float ClosestPickUpDistance = FVector::Distance(ClosestPickUp->GetActorLocation(), Player->GetActorLocation());

		for (int i{ 1 }; i < PickUps.Num(); ++i)
		{
			float PickUpDistance = FVector::Distance(PickUps[i]->GetActorLocation(), Player->GetActorLocation());
			if (PickUpDistance < ClosestPickUpDistance) {
				ClosestPickUpDistance = PickUpDistance;
				ClosestPickUp = PickUps[i];
			}
		}
	}
	
	return ClosestPickUp;
}

TArray<AELTPickUp*> AELTSpawner::GetPickUpsInView()
{
	TArray<AELTPickUp*> VisiblePickUps;
	if (PickUps.Num() > 0)
	{
		for (int i{ 0 }; i < PickUps.Num(); ++i)
		{
			if (PickUps[i]->WasRecentlyRendered())
			{
				VisiblePickUps.Add(PickUps[i]);
			}
		}
	}
	return VisiblePickUps;
}

TArray<AELTPickUp*> AELTSpawner::GetPickUpsOutOfView()
{
	TArray<AELTPickUp*> NotVisiblePickUps;
	if (PickUps.Num() > 0)
	{
		for (int i{ 0 }; i < PickUps.Num(); ++i)
		{
			if (!PickUps[i]->WasRecentlyRendered())
			{
				NotVisiblePickUps.Add(PickUps[i]);
			}
		}
	}
	return NotVisiblePickUps;
}

void AELTSpawner::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AELTSpawner, PickUps);
}